from django.contrib import admin
from django.urls import path, include
from RZD.pages import Views as MainViews
from material.frontend import urls as frontend_urls

urlpatterns = [
    path('admin/', admin.site.urls),
    path('app/', include(frontend_urls)),
] + MainViews.list()
